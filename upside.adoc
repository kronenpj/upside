= upsided(8) =
:doctype: manpage

== NAME ==
upsided - control software for the UPSide Uninterruptible Power Supply 

== SYNOPSIS ==
*upsided* [-s] [-w] [-p file] [-V] [-?]

== DESCRIPTION ==

upsided is the control software for the UPSide UPS.   To use it, you will
require a power plane built to UPSide specifications and a compatible
battery pack. This manual page exists to document upsided's external interfaces
and how to test-jig it.

== OPTIONS ==

Normally upsided runs in background as a service daemon, communicating
with the power plane and its display via i2c (control buttons are
attached to GPIO pins).  With the -s mode it acts as a filter,
transforming input event files 

With the -p option, upsided reads a TOML configuration from the named
file argument.

With the -w option, the daemon dumps its configuration to standard output and
exits.  That description is in TOML and can be modified.

With the -V option, upsided reports its version and exits.

With the -? option, upside prints an option summary and exits.

== DEVICE CLASSES AND NAMING CONVENTIONS ==

These are the things you need to know to read configuration and event files:

Load sensors are AC or DC voltage/current monitors speaking I2C.  The
DC sensor just after the AC-to-DC conversion on the mains feed is
named "mains".  Others are named "sens" with a zero-origin decimal
numeric suffix; these watch outlets.

Controls are I2C-actuated outlet switches. There is one for each "sens"
device; thus, "ctrl0", "ctrl1", etc.

One control/switch pair is designated "host"; it has priority on
available power and its load state has special significance to the
policy logic.

Battery management systems are named with a "bat" prefix.

There is a "display" device, the 20x4 LCD.

Buttons have IDs of the form "btn" with a following decimal integer.

This manual page deliberately does not specify the counts of device
types because they may differ among variants of the power plane.

== EVENT FILES ==

For diagnostic and testing purposes it is useful to have a standard
serialization of events in the UPSide system. An 'event file' is a
record of events that can be examined or payed back.  Synthetic
event files can be used as test cases for the daemon.

An event is represented by a text line of fields separated by
whitespace.  The first field of every event begins with a decimal
integer tick time in (roughly) nanoseconds. (In synthetic test logs
this field may be in arbitrary units) It is not guaranteed that
these correspond to any external timescale, as device time is not
NTP-regulated.

There is one event type for every sensor and control channel,
describing message traffic to and from the forebrain. There
is an additional type 'wait' representing the cycle delay in the
daemon's main polling loop. A line beginning with "#' is treated
as a comment and ignored.

The following table describes the payload of the second and later
fields of each record in an event file, following the timestamp.
exception

All fields are decimal integers unless otherwise specified.

Input log events: 

[options="header"]
|============================================================
|class   | Meaning                 | Fields
|ac      | AC load data            | ID, amperes AC
|dc      | DC load data            | ID, volts DC, amperes
|bms     | battery state data      | ID, BMS response type, variable data...
|btn     | button state            | ID, boolean
|host    | host message over USB   | "shutdown"
|wait    | end-of-cycle indication | None
|============================================================

At present, "shutdown" - which is host acknowledgment of a shutdown
request from UPSide - is the only defined message from the host. This
may change in the future.

Check log events:

[options="header"]
|============================================================
|class   | Meaning            | Fields
|alarm   | alarm sound        | string: alarm type
|display | display dump       | text shipped to display on following lines
|outlet  | outlet control     | ID, string: "on" or "off"
|event   | policy transition  | event-type -> state
|shutdown| shutdown           | shutdown attempt on host or UPS
|============================================================

BMS messages are as specified in section 5 of the SBS 1.1
standard.

This table lists the field breakdowns for SBS 1.1 messages that may be
included in an input event file.  Each subtype name is an SMS 1.1 function
name.  More may be added in the future.

[options="header"]
|===============================================================================
| BMS Message Subtype   | Fields
| AverageTimeToEmpty    | Time to battery empty.
| RelativeStateOfCharge | Charge percentage of current capacity
| AverageTimeToFull     | Time to battery full.
| AbsoluteStateOfCharge | Max charge as a percentage of designed max charge 
|===============================================================================

Maximum dwell time is computed when the battery can be observed at 100% charge.

== PROFILE == 

Some policy parameters control the daemon's behavior:

[options="header"]
|========================================================================
| Parameter            | Default | Meaning
| ShutdownTime         | 30 sec  | Time required for host shutdown
| SafeBootTime         | 60 sec  | Time required from host power on to
                                   ready for safe shutdown
| WarningTime          | 60 sec  | Desired warning margin before shutdown
| QuietTime            | 10ms    | Rest interval between groups of sensor polls
| LineVoltageThreshold | 0.90    | Fraction of voltage below which power
                                   is considered lost
| MasterSlaves         |         | Map of master sensors to lists of slave controls
| HostPair             | 0       | Which sens/ctrl pair is the host device 
|========================================================================

These can be set in /etc/upside-soft.rc using TOML syntax. Each line
is an the listed identifier, followed by "=", followed by a unitless
decimal value. There may be whitespace around the "=".

== REPORTING BUGS ==
Report bugs to Eric S. Raymond <esr@thyrsus.com>.  The project repository is
at https://gitlab.com/esr/upside

// Local Variables:
// mode: doc
// End:

