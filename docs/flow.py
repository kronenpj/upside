#!/usr/bin/env python3

# Generate things from pseudocode describing states and actions in UPSide.
# 1. The action-state diagram for the design document
# 2. Go code for the core state machine in the policy daemon.

import sys

class dot:
    "Output class for rendering to state diagram."
    def __init__(self, debug=False):
        self.debug = debug
        self.nstates = 0
        self.nactions = 0
    def begin(self):
        print('strict digraph {\n    size="10,20";')
    def end(self):
        print('}')
        sys.stderr.write("%d states, %d events, %d transitions.\n" \
                          % (self.nstates, len(events)-1, self.nactions))
    def state(self, node, text):
        if self.debug:
            text = '[' + node + ']\n' + text
        print('    "{0}" [shape=oval, label="{1}"]'.format(node, text))
        self.nstates += 1
    def action(self, fromstate, tostate, production, unreachable=False):
        label = "Trigger: %s\nMeans: %s" \
                % (events[production.trigger], production.meaning)
        if self.debug:
            label = '[' + production.name + ']\n' + label
        if production.action:
            label += "\n" + production.action
        arc = '    "{2}" [shape=box]; {0} -> "{2}" -> {1}'.format(fromstate, tostate, label)
        if unreachable:
            arc = '    edge [style=dotted]\n' + arc + '\n    edge [style=solid]'
        print(arc)
        self.nactions += 1

class flowgraph:
    def __init__(self):
        self.states = []
        self.transitions = []
        self.actions = []
        self.captions = {}
    def state(self, node, text):
        self.states.append((node, text))
        self.captions[node] = text
    def action(self, fromstate, tostate, production, unreachable=False):
        if unreachable: return
        self.actions.append((fromstate, tostate, production))
        if (production.name, production.trigger) not in self.transitions:
            self.transitions.append((production.name, production.trigger))
    def begin(self):
        pass

def dump_reverser(typename, keylist):
    sys.stdout.write("	reverse%s = map[%s]string{\n" % (typename, typename))
    for key in keylist:
        sys.stdout.write('		%s: "%s",\n' % (key, key))
    sys.stdout.write("	}\n")

def dump_stringer(typename):
    sys.stdout.write("""\
func (x %s) String() string {
	return reverse%s[x]
}

""" % (typename, typename))

class go_daemon(flowgraph):
    "Output class for rendering to Go source code."
    def __init__(self):
        flowgraph.__init__(self)
    def end(self):
        sys.stdout.write("""/*
 * transitions.go - policy state machine for the UPSide control daemon
 *
 * PROGRAM-GENERATED - DO NOT HAND-HACK!
 */

package main

type State int
type Event int

/*
 * Constants describing policy states.
 */
const (
""")
        for (i, (s, t)) in enumerate(self.states):
            sys.stdout.write("    %s State = %d	/* %s */\n" % (s, i, t.replace('\\n', ' ')))
        sys.stdout.write(""")

/*
 * Constants describing event types from the high-power subsystem.
 */
const (
""")
        for (i, (k, v)) in enumerate(events.items()):
            sys.stdout.write("    %s Event = %d	/* %s */\n" % (k, i, v))
        sys.stdout.write(""")

/*
 * Generated Stringer methods for constants 
 */

var reverseState map[State] string
var reverseEvent map[Event] string

func init () {
""")
        dump_reverser("State", [x[0] for x in self.states])
        dump_reverser("Event", events.keys())

        sys.stdout.write("}\n\n")

        dump_stringer("State")
        dump_stringer("Event")

        sys.stdout.write("""
var Readiness = %s

func Transition(event Event) {
\tswitch Readiness {
""" % self.states[0][0])
        for (s, t) in self.states:
            sys.stdout.write("\tcase %s:	/* %s */\n" % (s, t))
            sys.stdout.write("\t\tswitch event {\n")
            for (fromstate, tostate, production) in self.actions:
                if production.trigger == "EventUnreachable":
                    continue
                if fromstate == s:
                    sys.stdout.write("\t\tcase %s:\t/* %s */\n" % (production.trigger, events[production.trigger]))
                    sys.stdout.write("\t\t\t/* %s */\n" % production.meaning)
                    sys.stdout.write("\t\t\tReadiness = %s;\n" % tostate)
                    sys.stdout.write("\t\t\tReportTransition(event, Readiness)\n")

                    if production.hook:
                        sys.stdout.write("\t\t\t%s\n" % production.hook)
                    if production.alarm:
                        sys.stdout.write("\t\t\tSound(%s)\n" % production.tostate)
            sys.stdout.write("\t\t}\n")
        sys.stdout.write("""\t}
}
""")
        sys.stdout.write("/* end */\n")

class production:
    def __init__(self, name, trigger, meaning, alarm=None, action=None, hook=None):
        self.name = name
        self.trigger = trigger
        self.meaning = meaning
        self.alarm = alarm
        self.action = action
        self.hook = hook
    def alarmname(self):
        return "Alarm" + self.alarm.capitalize()
#
# Actual policy specification starts here.  Stuff before this point was
# generic machinery to compile the specification into output products.
#

# Triggers.
events = {
    "EventNone":	"No event", 
    "EventGoodAC":      "Poll of mains reports good AC voltage",
    "EventGoodBattery": "Poll of BMS reports TimeToEmpty > ShutdownTime + max(SafeBootTime + WarningTime)",
    "EventBadAC":       "Poll of mains reports bad AC voltage",
    "EventBatteryWarn": "Poll of BMS reports Time ToEmpty <= ShutdownTime + WarningTime",
    "EventBatteryLow":  "Poll of BMS reports TimeToEmpty <= ShutdownTime",
    "EventLoadLow":     "Poll of host load sensor shows current draw less than threshold,\nand no clean shutdown has ever been seen.",
    "EventHostShutdown":"Host reported clean shutdown",
    "EventUnreachable": "Code cannot reach this event",
    }


# Actions. Each action begins with a triggering event.  Some of these
# declare hooks to be called at specified transition.
CHARGING = production("Charging",
                      trigger="EventGoodAC",
                      meaning="Mains power is on")
CHARGED = production("Charged",
                     trigger="EventGoodBattery",
                     meaning="UPS is ready to power host",
                     action="Action: Enable AC outlets, bypass control input to high",
                     hook="EnableAllOutlets()")
MAINSOFF = production("MainsOff",
                      trigger="EventBadAC",
                      meaning="Mains power is off")
MAINSDROP = production("MainsDrop",
                       trigger="EventBadAC",
                       meaning="Mains power has dropped")
RESTORED = production("Restored",
                      trigger="EventGoodAC",
                      meaning="Power is restored")
DWELLWARNING = production("DwellWarning",
                          trigger="EventBatteryWarn",
                          meaning="Dwell limit approaching")
DWELLTIMEOUT = production("DwellTimeout",
                          trigger="EventBatteryLow",
                          meaning="Dwell limit imminent",
                          action="Action: Ship shutdown command to USB-monitoring host.",
                          hook="UPSide.ShutdownHost()")
BATTERYDRAIN = production("BatteryDrained",
                       trigger="EventUnreachable",
                       meaning="Battery is drained")
HOSTDOWN = production("HostShutsDown",
                      trigger="EventHostShutdown",
                      meaning="Host has shut down",
                      hook="DisableMasterOutlet()")
LOADGONE = production("UncleanDeath",
                      trigger="EventLoadLow",
                      meaning="Load has gone to zero without host shutdown",
                      hook="DisableMasterOutlet()")
RESTORED_LATE = production("RestoredLate",
                           trigger="EventGoodAC",
                           meaning="Power came back after host shut down")

if __name__ == '__main__':
    import sys, re, getopt
    go_generate = False
    debug = False
    (options, args) = getopt.getopt(sys.argv[1:], "gd")
    render = dot(debug=False)
    for (switch, val) in options:
        if switch == '-g':
            render = go_daemon()
        elif switch == '-d':
            render = dot(debug=True)

    # This is the pseudocode describing state-action transitions.
    #
    # Note: Some states could theoretically be coalesced.  HostDown
    # and DaemonUp, for example.  The trouble with going for an
    # absolutely minimal graph is that you end up with one that is
    # very difficult to read and think about.  This particular merge
    # puts DaemonRunning (the initial system state) at the bottom,
    # disrupting the useful expository property that normal flow of
    # control is (mostly) downwards.
    #
    # When you edit or propose edits to this graph, bear in mind that
    # state duplication does no actual harm because, as with a
    # conventional DFSA, the states don't do anything - it's the
    # transitions that fire actions and as long as those are right
    # the actual shape of the graph can vary around them.
    #
    # Actions marked "unreachable" are for the state-transition
    # diagram only, the code can never get there because they're
    # contingent on power cutting out while mains is unvailable..
    render.begin()
    render.state("DaemonUp", "Daemon running") 
    render.action("DaemonUp", "ChargeWait", CHARGING)
    render.state("ChargeWait", "Charge wait")
    render.action("ChargeWait", "MainsUp", CHARGED)
    render.action("ChargeWait", "OnBattery", MAINSDROP)
    render.state("MainsUp", "On mains power")
    render.action("DaemonUp", "OnBattery", MAINSOFF)
    render.action("MainsUp", "Overtime", DWELLWARNING)
    render.state("OnBattery", "On battery power")
    render.action("MainsUp", "OnBattery", MAINSDROP)
    render.action("OnBattery", "Overtime", DWELLWARNING)
    render.state("Overtime", "User warned of upcoming shutdown")
    render.action("Overtime", "PreShutdown", DWELLTIMEOUT)
    render.state("PreShutdown", "Awaiting power drop")
    render.action("PreShutdown", "ChargeWait", RESTORED)
    render.state("UPSCrash", "UPS goes dark")
    render.state("HostDown", "Host has shut down")
    render.action("PreShutdown", "HostDown", HOSTDOWN)
    render.action("PreShutdown", "HostDown", LOADGONE)
    render.action("PreShutdown", "UPSCrash", BATTERYDRAIN, unreachable=True)
    render.action("OnBattery", "ChargeWait", RESTORED)
    render.action("Overtime", "ChargeWait", RESTORED)
    render.action("HostDown", "ChargeWait", RESTORED_LATE)
    render.action("HostDown", "UPSCrash", BATTERYDRAIN, unreachable=True)
    render.end()

# end

